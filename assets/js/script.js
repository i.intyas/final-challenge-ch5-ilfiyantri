const hasil = document.getElementById("versus");
let isPlayable = true;

class Players {
    constructor(PlayerChoice, CompChoice) {
        this.PlayerChoice = PlayerChoice;
        this.CompChoice = CompChoice;
    }

    addPlayerChoice(pilihanOrang) {
        this.PlayerChoice[0] = pilihanOrang;
    }

    #mesin() {
        this.choices = ["cgunting", "cbatu", "ckertas"];
        this.CompChoice[0] = this.choices[Math.floor((Math.random() * 3))];

        if (this.CompChoice[0] === "cgunting") {
            document.getElementById("cgunting").style.backgroundColor = "rgba(255,255,255,.5)";
        }
        else if (this.CompChoice[0] === "cbatu") {
            document.getElementById("cbatu").style.backgroundColor = "rgba(255,255,255,.5)";
        }
        else {
            document.getElementById("ckertas").style.backgroundColor = "rgba(255,255,255,.5)";
        };

    }
    keputusan() {
        console.log(`pilihan kamu adalah  = ${this.PlayerChoice}`);
        console.log(`pilihan komputer adalah  = ${this.CompChoice}`)

        if ((this.PlayerChoice[0] === "pgunting" && this.CompChoice[0] === "ckertas") || (this.PlayerChoice[0] === "pbatu" && this.CompChoice[0] === "cgunting") || (this.PlayerChoice[0] === "pkertas" && this.CompChoice[0] === "cbatu")) {
            hasil.style.fontSize = "3rem";
            hasil.style.color = "white";
            hasil.style.paddingLeft = "1rem";
            hasil.style.paddingRight = "1rem";
            hasil.style.transform ="rotate(-30deg)";
            hasil.style.backgroundColor = "rgba(0,255,0,.2)";
            hasil.innerHTML = "PLAYER WIN";
            
        }
        else if ((this.PlayerChoice[0] === "pgunting" && this.CompChoice[0] === "cbatu") || (this.PlayerChoice[0] === "pbatu" && this.CompChoice[0] === "ckertas") || (this.PlayerChoice[0] === "pkertas" && this.CompChoice[0] === "cgunting")) {
            hasil.style.fontSize = "3rem";
            hasil.style.color = "white";
            hasil.style.paddingLeft = "1.7rem";
            hasil.style.paddingRight = "1.7rem";
            hasil.style.transform ="rotate(-30deg)";
            hasil.style.backgroundColor = "rgba(255,0,0,.2)";
            hasil.innerHTML = "COM WIN";
            
        }
        else {
            this.pemenang = "seri";
            hasil.style.fontSize = "3rem";
            hasil.style.color = "white";
            hasil.style.transform ="rotate(-30deg)";
            hasil.style.padding = "1.7rem";
            hasil.style.backgroundColor = "rgba(0,0,0,.2)";
            hasil.innerHTML = "SERI";
            
        }

    }
    pilihan() {
        const misalGunting = document.getElementById("pgunting")
        misalGunting.addEventListener("click", () => {
            if (isPlayable) {
                isPlayable = false;
                misalGunting.style.backgroundColor = "rgba(255,255,255,.5)";
                const pilihanOrang = 'pgunting';
                this.addPlayerChoice(pilihanOrang);
                setTimeout(() => {
                    this.#mesin();
                    this.keputusan();
                }, 500)
            }

        });


        const misalBatu = document.getElementById("pbatu")
        misalBatu.addEventListener("click", () => {
            if (isPlayable) {
                isPlayable = false;
                misalBatu.style.backgroundColor = "rgba(255,255,255,.5)";
                const pilihanOrang = 'pbatu';
                this.addPlayerChoice(pilihanOrang);
                setTimeout(() => {
                    this.#mesin();
                    this.keputusan();
                }, 500)
            }
        });

        const misalKertas = document.getElementById("pkertas")
        misalKertas.addEventListener("click", () => {
            if (isPlayable) {
                isPlayable = false;
                misalKertas.style.backgroundColor = "rgba(255,255,255,.5)";
                const pilihanOrang = 'pkertas';
                this.addPlayerChoice(pilihanOrang);
                setTimeout(() => {
                    this.#mesin();
                    this.keputusan();
                }, 500)
            }
        });

        const ulang = document.getElementById("refresh")
        ulang.addEventListener("click", () => {
            history.go(0);
            isPlayable = true;
        });
    }
}

const jalan = new Players([], []);
jalan.pilihan();
