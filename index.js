const express = require("express");
const bodyParse = require("body-parser");
const path = require('path')
let posts = require("./data/users.json")
const app = express();
const port = 8080;

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname,'assets')));
app.use(bodyParse.json());


app.get('/', (req,res) => {
    res.render('landing/index.ejs')
});

app.get('/home', (req,res) => {
    res.render('landing/index.ejs')
});

app.get('/playnow', (req,res) => {
    res.render('game/index.ejs')
});

app.get('/datauser', (req,res) => {
    res.status(200).json(posts)
}); 


//checking data  user existance
app.post('/login', (req,res) => {
    const user = posts.find(user => user.email === req.body.email)
    if (user){
        const isMatch = user.password === req.body.password;
        if (isMatch) {
            res.send(user);
        } else {
            res.status(404).send ("PASSWORD NOT VALID")
        }
    } else {
        res.status(404).send ("EMAIL NOT VALID")
    }
})

app.listen(port,() => console.log(`example app listening at ${port}`))


// //DELTE DATA USER DI JASON
// app.delete('/delete',(req,res) => {
//     const deleteUser = posts.find(data => data.email = req.body.email);
//     res.send(posts);
// })
